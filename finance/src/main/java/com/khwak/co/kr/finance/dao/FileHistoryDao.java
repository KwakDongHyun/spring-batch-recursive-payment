package com.khwak.co.kr.finance.dao;

import lombok.Data;

@Data
public class FileHistoryDao {

    private String bnkC;        // 금융사코드
    private String prcDsc;      // 파일처리구분코드
    private String createDate;  // 생성일자
    private String fileCode;    // 파일종류코드 ex) A전문, B전문, etc.

}
