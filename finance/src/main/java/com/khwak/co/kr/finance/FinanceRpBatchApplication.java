package com.khwak.co.kr.finance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class FinanceRpBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinanceRpBatchApplication.class, args);
	}

}
