package com.khwak.co.kr.finance.task;

import com.khwak.co.kr.finance.dao.FileHistoryDao;
import com.khwak.co.kr.finance.service.FinancialCorporation;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@ConfigurationProperties(prefix = "serviceList")
public class FinCorpFilePrcTask {

    private final FinancialCorporation financialCorporation;

    /**
     * 금융사별로 van사로 송신할 파일을 만드는 작업.
     */
    @Scheduled(cron = "${cron.fileMake}")
    public void FileMakeTask() {

    }

    /**
     * van사로부터 수신할 파일을 만드는 작업
     */
    @Scheduled(cron = "${cron.fileRead}")
    public void FileReadTask() {
        /*
         *Todo
         * DB에서 금일 수신된 파일 중 미처리된 파일 리스트를 조회.
         * Field에 금융사코드가 들어가므로 가능함.
         */
        List<FileHistoryDao> fileHistoryList = new ArrayList<>();
        FileHistoryDao fileHistoryDao = new FileHistoryDao();
        fileHistoryDao.setBnkC("HDC");
        fileHistoryDao.setFileCode("1");
        fileHistoryDao.setPrcDsc("0");
        fileHistoryDao.setCreateDate("20221008");
        fileHistoryList.add(fileHistoryDao);

        fileHistoryDao = new FileHistoryDao();
        fileHistoryDao.setBnkC("SHB");
        fileHistoryDao.setFileCode("2");
        fileHistoryDao.setPrcDsc("0");
        fileHistoryDao.setCreateDate("20221010");
        fileHistoryList.add(fileHistoryDao);

        /*
         *Todo
         * 금융사에서 처리.
         */
    }

}
